all:    client server source_image source_screenshot_xlib

consoleclient:
	g++ -pthread scr_client.cc -o scr_consoleclient -Iextra/cimg -Wall -Wextra -Wfatal-errors \
		-std=c++11 -pedantic -Dcimg_use_vt100 -Dcimg_display=1    \
		-lm -lX11   -lpthread -L/usr/lib -ljpeg
client:
	g++ -pthread scr_client.cc -o scr_client -DSDL=1 -Iextra/cimg -Wall -Wextra -Wfatal-errors \
		-std=c++11 -pedantic -Dcimg_use_vt100 -Dcimg_display=1    \
		-lm -lX11   -lpthread -L/usr/lib -ljpeg -lSDL2

server:
	g++ -pthread scr_server.cc -o scr_server -ldl \
			-Iextra/cimg -Wall -Wextra -Wfatal-errors -std=c++11 -pedantic -Dcimg_use_vt100 \
			-Dcimg_display=1 -lm -lX11 -lpthread -L/usr/lib -ljpeg

source_image:
	g++ -pthread image.cc -o image.so -fPIC -shared \
			-Iextra/cimg -Wall -Wextra -Wfatal-errors -std=c++11 -pedantic -Dcimg_use_vt100 \
			-Dcimg_display=1 -lm -lX11 -lpthread -L/usr/lib -ljpeg

source_screenshot_xlib:	
	g++ -pthread screenshot_xlib.cc -o screenshot_xlib.so -fPIC -shared \
			-Iextra/cimg -Wall -Wextra -Wfatal-errors -std=c++11 -pedantic -Dcimg_use_vt100 \
			-Dcimg_display=1 -lm -lX11 -lpthread -L/usr/lib -ljpeg
	

clean:
	rm -f scr
	rm -f scr_server
	rm -f scr_client
	rm -f scr_consoleclient
	rm -f sdlrender
	rm -f screenshot.jpg
	rm -f screenshot_print.jpg
	rm -f scr_mem
	rm -f screenshot_mem.jpg
	rm -f screenshot_dump
	rm -f client_image_dump
	rm -f server_image_dump
	rm -f server_imagebuffer_dump
	rm -f image.so
	rm -f screenshot_xlib.so
