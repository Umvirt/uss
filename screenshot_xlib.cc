// Server side implementation of UDP client-server model 
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <netinet/in.h> 

//time
#define _POSIX_C_SOURCE 200809L

#include <inttypes.h>
#include <math.h>
#include <time.h>

#include "scr.h"
#include "imagesource.h"

//variables
#define APP_VERSION     "0.2"
#define PORT     8888 
#define MAXLINE 1024 

#define _POSIX_C_SOURCE 200809L

#include <inttypes.h>
#include <math.h>
#include <stdio.h>
#include <time.h>

//screen

#include <X11/Xlib.h>
#include <X11/X.h>

#include <stdio.h>
#include <X11/Xutil.h>


//jpg cimg plugin
#include <jpeglib.h>
#include <jerror.h>
#define cimg_plugin "plugins/jpeg_buffer.h"
#include "CImg.h"

#include <getopt.h>

using namespace cimg_library;

extern "C" ImageSource* create_object()
{
  return new ImageSource;
}

extern "C" void destroy_object( ImageSource* object )
{
  delete object;
}

ImageSource::ImageSource()
{

}

void ImageSource::init(char * source_parameters, unsigned char * settings)
{
	//load source parameters  
	parameters = source_parameters;
	//load app settings
	memcpy(&app_settings, settings, sizeof(settings));

	//Show recieved settings
	if(app_settings.verbose>0){
		printf("Passed to shared module data:\n");
		printf("JPEG-compression: %d\n", app_settings.jpeg_quality);
		printf("Verbose: %d\n", app_settings.verbose);
		printf("Channels: %d\n", app_settings.channels);
		printf("Weighted gray: %d\n", app_settings.weighted_gray);
		printf("Source parameters: %s\n", parameters);
	}


}

void ImageSource::getImage(JOCTET ** buffer_output, unsigned int ** buf_size)
{
	//Environment interfaces init	
	Display *display = XOpenDisplay(NULL);
	Window root = DefaultRootWindow(display);
	XWindowAttributes gwa;
	XGetWindowAttributes(display, root, &gwa);

	//Get parameters
	int width = gwa.width;
	int height = gwa.height;
	
	//Debug: show image parameters
	if(app_settings.verbose>0){
		printf(" width=%d",width);
		printf(" height=%d",height);
		printf(" channels=%d",app_settings.channels);
	}
	//Creating screenshot image
	XImage *image = XGetImage(display,root, 0,0 , width,height,AllPlanes, ZPixmap);

	//Image matrix (Cimg source data) init
		//?BUG (dynamic allocation):
		//unsigned char *array = new unsigned char[width * height * channels];
		//?Bugfix (static allocation):
		//unsigned char array[1000000];
	uint8_t * array ;
	array= (uint8_t *) malloc( width * height * app_settings.channels );

	//Processing screenshot and creating image matrix
	unsigned char R,G,B; //chanel values
	unsigned long c=0; //image matrix pixel index
	//Processing rows (y-axis)
	for (int y = 0; y < height ; y++){
		//Processing cols (x-axis)
		for (int x = 0; x < width; x++){
			//Fetch pixel value from screenshot image
			unsigned long pixel = XGetPixel(image,x,y);
			
			//increasing pixel index	
			c++;		

			//if rgb mode
			if(app_settings.channels==3){
				array[c+(height*width)*2]=pixel;
				array[c+(height*width)*1]=pixel>>8;
				array[c+(height*width)*0]=pixel>>16;
			}
		
			//if grayscale
			if(app_settings.channels==1){
				R=pixel;
				G=pixel>>8;
				B=pixel>>16;

				if(app_settings.weighted_gray){
					array[c]=round(0.299*R)+round(0.587*G)+round(0.114*B);
				}else{
					array[c]=round(0.3*R)+round(0.3*G)+round(0.3*B);
				}
			}
		}
	}

	//Processing end	
	
	//CImg buffer size
	unsigned buf_size_ = 50000000; 
	//CImg buffer memory
	*buffer_output = new JOCTET[2*buf_size_];

	//Debug: loadning CImg contents from jpeg-file
	//CImg<unsigned char> pic("test.jpg");
	CImg<unsigned char> pic(array,width,height,1,app_settings.channels);
	//Debug: saving CImg contents to jpeg-file   
	/*
	std::fprintf(stderr," - Save CImg to file\n",filename_output);
	pic.save_jpeg("screenshot.jpg",jpeg_quality);
	*/

	//Saving CImg to buffer
	pic.save_jpeg_buffer(*buffer_output, buf_size_,app_settings.jpeg_quality);

	//Debug: saving CImg contents to jpeg-file   
	/*
	const char *filename_output = "screenshot_print.jpg";
	std::fprintf(stderr," - Save output file '%s'\n",filename_output);
	std::FILE* file_output = std::fopen(filename_output,"wb");
	std::fwrite(*buffer_output, sizeof(JOCTET), buf_size_, file_output);
	std::fclose(file_output);
	*/

	//Debug: show size
	if(app_settings.verbose>0){
		printf(" size=%d\n",buf_size_);
	}	
	//storing buffer size to argument	
	*buf_size=&buf_size_;
	
	//Cleanup	
	XDestroyImage( image );
	XCloseDisplay(display);
	delete[] array;
}
