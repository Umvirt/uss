// Server side implementation of UDP client-server model 
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <netinet/in.h> 

//time
#define _POSIX_C_SOURCE 200809L

#include <inttypes.h>
#include <math.h>
#include <time.h>

#include "scr.h"
#include "imagesource.h"

//variables
#define APP_VERSION     "0.2"
#define PORT     8888 
#define MAXLINE 1024 

#define _POSIX_C_SOURCE 200809L

#include <inttypes.h>
#include <math.h>
#include <stdio.h>
#include <time.h>

//screen

#include <X11/Xlib.h>
#include <X11/X.h>

#include <stdio.h>
#include <X11/Xutil.h>


//jpg cimg plugin
#include <jpeglib.h>
#include <jerror.h>
#define cimg_plugin "plugins/jpeg_buffer.h"
#include "CImg.h"

#include <getopt.h>

using namespace cimg_library;

extern "C" ImageSource* create_object()
{
  return new ImageSource;
}

extern "C" void destroy_object( ImageSource* object )
{
  delete object;
}

ImageSource::ImageSource()
{

}

void ImageSource::init(char * source_parameters, unsigned char * settings)
{
	//load source parameters  
	parameters = source_parameters;
	//load app settings
	memcpy(&app_settings, settings, sizeof(settings));

	//Show recieved settings
	if(app_settings.verbose>0){
		printf("Passed to shared module data:\n");
		printf("JPEG-compression: %d\n", app_settings.jpeg_quality);
		printf("Verbose: %d\n", app_settings.verbose);
		printf("Channels: %d\n", app_settings.channels);
		printf("Weighted gray: %d\n", app_settings.weighted_gray);
		printf("Source parameters: %s\n", parameters);
	}


}

void ImageSource::getImage(JOCTET ** buffer_output, unsigned int ** buf_size)
{
	unsigned buf_size_ = 5000000; 
	*buffer_output = new JOCTET[2*buf_size_];
	//default_image	
	char * file="";
	//printf("%s\n",parameters);
	//printf("%d\n",sizeof(parameters));
	//If parameters value passed
	//if(sizeof(parameters)>8){
		//use value as filename
		file=parameters;
	//}
	
	CImg<unsigned char> pic(file);

	pic.save_jpeg_buffer(*buffer_output, buf_size_,app_settings.jpeg_quality);

	*buf_size=&buf_size_;
}
