// Server side implementation of UDP client-server model 
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <netinet/in.h> 

//time
#define _POSIX_C_SOURCE 200809L

#include <inttypes.h>
#include <math.h>
#include <time.h>

#include "scr.h"
#include "imagesource.h"

//variables
#define APP_VERSION     "0.3"
#define PORT     8888 
#define MAXLINE 1024 

#define _POSIX_C_SOURCE 200809L

#include <inttypes.h>
#include <math.h>
#include <stdio.h>
#include <time.h>

//screen

#include <X11/Xlib.h>
#include <X11/X.h>

#include <stdio.h>
#include <X11/Xutil.h>


//jpg cimg plugin
#include <jpeglib.h>
#include <jerror.h>
#define cimg_plugin "plugins/jpeg_buffer.h"
#include "CImg.h"

#include <getopt.h>


#include <dlfcn.h>

//file_exists
#include <sys/stat.h>
#include <unistd.h>
#include <string>
#include <fstream>



//global variables
unsigned int jpeg_quality = 10;
//channels: 3 - RGB 1 - Grayscale
unsigned int channels = 1;
//weighted gray
bool weighted_gray=0;
//verbose mode
bool verbose=0;
//binging to port
unsigned int port=PORT;
//udp mode
bool udp_mode=0;
//sleep
unsigned int sleeptime=0;
//source settings
char * source="";
//source settings
char * source_parameters="";

//DSO
ImageSource* DSOSource;



using namespace cimg_library;



inline bool file_exists (const std::string& name) {
  struct stat buffer;   
  return (stat (name.c_str(), &buffer) == 0); 
}



void timestamp (time_t &s, long &ms)
{
    //long            ms; // Milliseconds
    //time_t          s;  // Seconds
    struct timeval spec;

 //   clock_gettime(CLOCK_REALTIME, &spec);
gettimeofday(&spec, NULL);
    s  = spec.tv_sec;
    ms = spec.tv_usec;
//round(spec.tv_usec / 1.0e6); // Convert nanoseconds to milliseconds
    /*if (ms > 999) {
        s++;
        ms = 0;
    }*/

   return;

 //   printf("Current time: %"PRIdMAX".%03ld seconds since the Epoch\n",
 //          (intmax_t)s, ms);
}


/**
* Get image from 'test.jpg' contents
*/
int plug(JOCTET ** buffer_output, unsigned int ** buf_size)
{
	unsigned buf_size_ = 5000000; 
	*buffer_output = new JOCTET[2*buf_size_];
	CImg<unsigned char> pic("test.jpg");

	pic.save_jpeg_buffer(*buffer_output, buf_size_,jpeg_quality);
	
	if(verbose){
		printf("size: %d\n",buf_size_);
		printf("size addr: %d\n",&buf_size_);
	}

	*buf_size=&buf_size_;
}

/**
* Get image from screenshot via Xlib
*/
int screenshot(JOCTET ** buffer_output, unsigned int ** buf_size)
{
	//Environment interfaces init	
	Display *display = XOpenDisplay(NULL);
	Window root = DefaultRootWindow(display);
	XWindowAttributes gwa;
	XGetWindowAttributes(display, root, &gwa);

	//Get parameters
	int width = gwa.width;
	int height = gwa.height;
	
	//Debug: show image parameters
	if(verbose){
		printf(" width=%d",width);
		printf(" height=%d",height);
		printf(" channels=%d",channels);
	}
	//Creating screenshot image
	XImage *image = XGetImage(display,root, 0,0 , width,height,AllPlanes, ZPixmap);

	//Image matrix (Cimg source data) init
		//?BUG (dynamic allocation):
		//unsigned char *array = new unsigned char[width * height * channels];
		//?Bugfix (static allocation):
		//unsigned char array[1000000];
	uint8_t * array ;
	array= (uint8_t *) malloc( width * height * channels );

	//Processing screenshot and creating image matrix
	unsigned char R,G,B; //chanel values
	unsigned long c=0; //image matrix pixel index
	//Processing rows (y-axis)
	for (int y = 0; y < height ; y++){
		//Processing cols (x-axis)
		for (int x = 0; x < width; x++){
			//Fetch pixel value from screenshot image
			unsigned long pixel = XGetPixel(image,x,y);
			
			//increasing pixel index	
			c++;		

			//if rgb mode
			if(channels==3){
				array[c+(height*width)*2]=pixel;
				array[c+(height*width)*1]=pixel>>8;
				array[c+(height*width)*0]=pixel>>16;
			}
		
			//if grayscale
			if(channels==1){
				R=pixel;
				G=pixel>>8;
				B=pixel>>16;

				if(weighted_gray){
					array[c]=round(0.299*R)+round(0.587*G)+round(0.114*B);
				}else{
					array[c]=round(0.3*R)+round(0.3*G)+round(0.3*B);
				}
			}
		}
	}
	//Processing end	
	
	//CImg buffer size
	unsigned buf_size_ = 50000000; 
	//CImg buffer memory
	*buffer_output = new JOCTET[2*buf_size_];

	//Debug: loadning CImg contents from jpeg-file
	//CImg<unsigned char> pic("test.jpg");
	CImg<unsigned char> pic(array,width,height,1,channels);
	//Debug: saving CImg contents to jpeg-file   
	/*
	std::fprintf(stderr," - Save CImg to file\n",filename_output);
	pic.save_jpeg("screenshot.jpg",jpeg_quality);
	*/

	//Saving CImg to buffer
	pic.save_jpeg_buffer(*buffer_output, buf_size_,jpeg_quality);

	//Debug: saving CImg contents to jpeg-file   
	/*
	const char *filename_output = "screenshot_print.jpg";
	std::fprintf(stderr," - Save output file '%s'\n",filename_output);
	std::FILE* file_output = std::fopen(filename_output,"wb");
	std::fwrite(*buffer_output, sizeof(JOCTET), buf_size_, file_output);
	std::fclose(file_output);
	*/

	//Debug: show size
	if(verbose){
		printf(" size=%d\n",buf_size_);
	}	
	//storing buffer size to argument	
	*buf_size=&buf_size_;
	
	//Cleanup	
	XDestroyImage( image );
	XCloseDisplay(display);
	delete[] array;
}

void helpdump(){
	fprintf(stdout,"UmVirt Screen Server usage: \n\n");
	fprintf(stdout,"	scr_server <options> \n\n");
	fprintf(stdout,"Options: \n");
	fprintf(stdout,"--help                          Print this page.\n");
	fprintf(stdout,"--version                       Print version.\n");
	fprintf(stdout,"--verbose                       Verbose mode.\n");
	fprintf(stdout,"--udp-mode                      Connect via UDP against TCP.\n");
	fprintf(stdout,"--source=MODULE                 Image source (plug-in module) to use.\n");
	fprintf(stdout,"--source-parameters=STRING      Image source parameters string.\n");
	fprintf(stdout,"--port=PORT                     Bind to port. (Default: 8888)\n");
	fprintf(stdout,"--channels=<1|3>                Image chanels <1|3> (grayscale|RGB).\n");
	fprintf(stdout,"--weighted-gray                 Weighted gray RGB to grayscale conversion.\n");
	fprintf(stdout,"--jpeg-compression=<1..100>     JPEG compression level.\n");
	fprintf(stdout,"--sleep=SECONDS                 Sleep seconds between frames.\n");
	exit(EXIT_SUCCESS);
}


void printversion(){
	fprintf(stdout,"UmVirt Screen Server v.%s\n",APP_VERSION);
	exit(EXIT_SUCCESS);
}

/**
* Streaming transaction
*/
void streamer(int * sockfd, struct sockaddr_in * cliaddr){
//	int frame=0;


	//unsigned int n; 
	//socklen_t len = sizeof cliaddr;
   // n = recvfrom(sockfd, (char *)buffer, MAXLINE,  
   //             MSG_WAITALL, ( struct sockaddr *) &cliaddr, 
    //            &len); 
   // buffer[n] = '\0'; 

// if (send(sockfd, "Hello, world!", 13, 0) == -1)
//                perror("send");
//            close(sockfd);




//return(0);
//sockfd = accept(tsockfd, NULL, NULL);
//return(0);
    
	//Response handling  

	socklen_t len = sizeof *cliaddr;
/*
    n = recvfrom(sockfd, (char *)buffer, MAXLINE,  
                MSG_WAITALL, ( struct sockaddr *) &cliaddr, 
                &len); 
    buffer[n] = '\0'; */
	//Debug: Client output
 	//printf("Client : %s\n", buffer); 
	
	//Sleep for 5 seconds
	//sleep(5);

	//Frame counter
	int frame=0;

    while(1){
		frame++;
		if(verbose){
			printf("Frame #%d:",frame);
		}
//    sprintf(response, "%ld\n",timestamp());

		unsigned *buf_size_;
		unsigned xsize;
		//image file buffer
		JOCTET *buffer_output;

		//Image from screenshot
		//screenshot(&buffer_output,&buf_size_);
		//Image from file
		//plug(&buffer_output,&buf_size_);
		
		//Image from ImageSource
				
DSOSource->getImage(&buffer_output,&buf_size_);
		
        //saving buffer size value
		xsize=*buf_size_;

		//If file size more than rec.buffer size
		if(*buf_size_>sizeof(record::buffer)){

			//Performing segmentation

			//printf("size: %d\n",xsize);
			if(verbose){
				printf("Warning: Image size is to BIG!: %d\n",xsize );
			}
			int scount=(1+floor(xsize/sizeof(record::buffer)));
			if(verbose){			
				printf("Spliting in %d segments...\n",scount);
			}
			//return 0; 

			//Debug dump file buffer content
			/*			
			const char *filename_output = "foo_output.jpg";
			std::fprintf(stderr," - Save output file '%s'\n",filename_output);
			std::FILE* file_output = std::fopen(filename_output,"wb");
			std::fwrite(buffer_output, sizeof(JOCTET), xsize, file_output);
			std::fclose(file_output);
			*/
	
			JOCTET tempo[2*SCR_TBUFFER_SIZE];

int i;
			unsigned long int j,p;
			unsigned char bc;
			p=0; //cursor

			//Filling segments by data
			//Processing each segment
			for(i = 0; i < scount; i++){
				//Processing each byte in proper buffer portion
				for(j = 0; j < SCR_TBUFFER_SIZE; j++){
					//If cursor out of range					
					if(p>=xsize){
						//exit from j-loop, continue i-loop
						break;		
					}
					
					//simple copy of byte		
					bc=buffer_output[p];
					tempo[j]=bc;
					
					//Cursor increasing 
					++p;
				}

				struct record rec;

				timestamp(rec.time, rec.mtime);
				//printf("T:%d-%d\n", rec.time, rec.mtime);

				rec.total_size=xsize;
				rec.buffer_size=SCR_TBUFFER_SIZE;
				rec.final=0;
				//If last segment
				if(i==scount-1){
					rec.final=1;
					rec.buffer_size=xsize-(SCR_TBUFFER_SIZE*(scount-1));
				}
				//Fill buffer value
				memcpy(rec.buffer,tempo,rec.buffer_size);
				//Stroring segment index
				rec.index=i;

				//Sending segment

				//UDP MODE (Non-confirmation, flood)
				if(udp_mode){   
					sendto(*sockfd, (const char *) &rec, sizeof(rec),  
					MSG_DONTWAIT, (const struct sockaddr *) cliaddr, 
					len);
//printf("ADDR: %d\n",servaddr.sin_addr.s_addr)
//	printf("SOCK: %d\n",*sockfd);

//sleep(5);
				//TCP MODE (Confirmation)
				}else{
					sendto(*sockfd, (const char *) &rec, sizeof(rec),  
					MSG_CONFIRM, (const struct sockaddr *) cliaddr, 
					len);
				}

				

				//sleep(5);
				if(verbose){

					printf("Segment #%d:",rec.index );
					printf(" segment_size=%d",rec.buffer_size );
					printf(" total_size=%d\n",rec.total_size );
				}
				//Debug: Segmentation file dump 
				/*
				char filename_output [10];
				sprintf(filename_output,"%d.dump", i);
				std::FILE* file_output = std::fopen(filename_output,"wb");
				std::fwrite(&rec, sizeof(rec),1, file_output);
				std::fclose(file_output);
				*/













			}

			//Sending data
			//Processing each segment
		//	for(i = 0; i < scount; i++){

				
		//	}

/*		
			//Creating segments
			JOCTET segments[scount][2*SCR_TBUFFER_SIZE];
	
			int i;
			unsigned long int j,p;
			unsigned char bc;
			p=0; //cursor

			//Filling segments by data
			//Processing each segment
			for(i = 0; i < scount; i++){
				//Processing each byte in proper buffer portion
				for(j = 0; j < SCR_TBUFFER_SIZE; j++){
					//If cursor out of range					
					if(p>=xsize){
						//exit from j-loop, continue i-loop
						break;		
					}
					
					//simple copy of byte		
					bc=buffer_output[p];
					segments[i][j]=bc;
					
					//Cursor increasing 
					++p;
				}
			}

			//Sending data
			//Processing each segment
			for(i = 0; i < scount; i++){

				struct record rec;

				rec.time=timestamp();
				rec.total_size=xsize;
				rec.buffer_size=SCR_TBUFFER_SIZE;
				rec.final=0;
				//If last segment
				if(i==scount-1){
					rec.final=1;
					rec.buffer_size=xsize-(SCR_TBUFFER_SIZE*(scount-1));
				}
				//Fill buffer value
				memcpy(rec.buffer,segments[i],rec.buffer_size);
				//Stroring segment index
				rec.index=i;

				//Sending segment
				sendto(sockfd, (const char *) &rec, sizeof(rec),  
				MSG_DONTWAIT, (const struct sockaddr *) &cliaddr, 
				len);
				//sleep(5);
				if(verbose){

					printf("Segment #%d:",rec.index );
					printf(" segment_size=%d",rec.buffer_size );
					printf(" total_size=%d\n",rec.total_size );
				}
				//Debug: Segmentation file dump 
				//-*
				char filename_output [10];
				sprintf(filename_output,"%d.dump", i);
				std::FILE* file_output = std::fopen(filename_output,"wb");
				std::fwrite(&rec, sizeof(rec),1, file_output);
				std::fclose(file_output);
				// *-/
			}
*/

		//If image size lower than or equal to rec.buffer size
		}else{

			//Debug: Image content buffer file dump 
			/*
			const char *filename_output = "server_imagebuffer_dump";
			std::FILE* file_output = std::fopen(filename_output,"wb");
			std::fwrite(buffer_output, *buf_size_,1, file_output);
			std::fclose(file_output);
			*/

			/*
			  const char *filename_input = "test.jpg";
			  std::fprintf(stderr," - Reading file '%s'\n",filename_input);
			  std::FILE *file_input = std::fopen(filename_input,"rb");
			  if (!file_input) { std::fprintf(stderr,"Input JPEG file not found !"); std::exit(0); }

			  std::fprintf(stderr," - Construct input JPEG-coded buffer\n");
			  unsigned buf_size = 500000; // Put the file size here !
			  JOCTET *buffer_input = new JOCTET[buf_size];
			  if (std::fread(buffer_input,sizeof(JOCTET),buf_size,file_input)) std::fclose(file_input);
			  // -> 'buffer_input' is now a valid jpeg-coded memory buffer.

			  buffer_output=buffer_input;
			  *buf_size_=buf_size;
			*/

			//printf("%s\n",reinterpret_cast<char*>(buffer_output));
			//printf(">> %d << \n",*buf_size_);

			struct record rec;
			timestamp(rec.time, rec.mtime);;

			memcpy(rec.buffer, buffer_output,*buf_size_);
			rec.buffer_size=*buf_size_;
			rec.total_size=*buf_size_;
			rec.final=1;

//rec.time=0
//rec.mtime=0;
//printf("T:%d-%d\n", rec.time, rec.mtime);
			//Debug: Image content buffer file dump 
			/*
			const char *filename_output = "server_image_dump";
			std::FILE* file_output = std::fopen(filename_output,"wb");
			std::fwrite(&rec, sizeof(rec),1, file_output);
			std::fclose(file_output);
			*/

			//printf("SIZE of struct: >> %d << \n",sizeof(rec));
			//printf("SIZE of buffer: >> %d << \n",sizeof(rec.buffer));

			
				//UDP MODE (Non-confirmation, flood)
				if(udp_mode){   
					sendto(*sockfd, (const char *) &rec, sizeof(rec),  
					MSG_DONTWAIT, (const struct sockaddr *) cliaddr, 
					len);
				//TCP MODE (Confirmation)
				}else{
					sendto(*sockfd, (const char *) &rec, sizeof(rec),  
					MSG_CONFIRM, (const struct sockaddr *) cliaddr, 
					len);
				}

//sendto(*sockfd, (const char *) &rec, sizeof(rec),  
//			MSG_DONTWAIT, (const struct sockaddr *) &cliaddr, 
	//		len);

			//Dummy:
			//     sendto(sockfd, (const char *)"12345", 3,  
			//        MSG_DONTWAIT, (const struct sockaddr *) &cliaddr, 
			//            len); 

	}
	delete[] buffer_output;

//printf("Hello message sent.\n");  
  //return 0;
if(sleeptime){
	sleep(sleeptime);  
	}
}

}

int main(int argc, char *argv[])
{
		   int c;
           int digit_optind = 0;

           while (1) {
               int this_option_optind = optind ? optind : 1;
               int option_index = 0;
               static struct option long_options[] = {
                   {"weighted-gray",  no_argument, 0, 'w'},
				   {"version",     no_argument, 0,  'v' },
                   {"help",  no_argument,       0,  'H' },
                   {"channels",  required_argument, 0,  'c' },
				   {"sleep",  required_argument, 0,  's' },
				   {"port",  required_argument, 0,  'p' },
				   {"source",  required_argument, 0,  'S' },
				   {"source-parameters",  required_argument, 0,  'P' },
                   {"verbose", no_argument,       0,  'V' },
                   {"udp-mode", no_argument,       0,  'u' },
                   {"jpeg-compression",   required_argument, 0, 'j'},
                   {0,         0,                 0,  0 }
               };

               c = getopt_long(argc, argv, "vcjwpVPS:",
                        long_options, &option_index);
               if (c == -1)
                   break;

               switch (c) {
               case 0:
                   printf("option %s", long_options[option_index].name);
                   if (optarg)
                       printf(" with arg %s", optarg);
                   printf("\n");
                   break;

         /*      case '0':
               case '1':
               case '2':
                   if (digit_optind != 0 && digit_optind !=
       this_option_optind)
                     printf("digits occur in two different argv-ele‐
       ments.\n");
                   digit_optind = this_option_optind;
                   printf("option %c\n", c);
                   break;
*/
               case 'a':
                   printf("option a\n");
                   break;

				case 'v':
					printversion();
					
				break;               
				 case 'V':
                   verbose = 1;
                   break;				
				case 'w':
                   weighted_gray=1;
                   break;
				case 'u':
                   udp_mode=1;
                   break;



               case 'c':
					channels=atoi(optarg);
                   break;

               case 'j':
                   jpeg_quality = atoi(optarg);
                   break;
			 case 'p':
                   port = (unsigned int)atoi(optarg);
                   break;
			 case 'S':
                   source = optarg;
                   break;
			 case 'P':
                   source_parameters = optarg;
                   break;
			 case 's':
                   sleeptime = (unsigned int)atoi(optarg);
                   break;

			case 'H':
               case '?':
				   helpdump();
                   break;

               default:
                   printf("?? getopt returned character code 0%o ??\n", c);
               }
           }

	//DSO check
	char dso[255];
	if(source==""){
		perror("Error: Source not defined\n");
		exit(EXIT_FAILURE); 
	}

	
	sprintf(dso,"./%s.so",source);

	if(verbose){
		printf("Source: %s\n",source);
		printf("Loading source module: %s\n",dso);
		printf("Source module existance: %d\n",file_exists(dso));
	}
	//DSO file check
	if(!file_exists(dso)){
		perror("Error: Source module file not found\n");
		exit(EXIT_FAILURE); 
	}


//exit(EXIT_FAILURE);
	/* on Linux, use "./myclass.so" */
//	void* handle = dlopen("./image.so", RTLD_LAZY);
	void* handle = dlopen(dso, RTLD_LAZY);

    ImageSource* (*create)();
    void (*destroy)(ImageSource*);

    create = (ImageSource* (*)())dlsym(handle, "create_object");
    destroy = (void (*)(ImageSource*))dlsym(handle, "destroy_object");

    DSOSource = (ImageSource*)create();
	
	//container to pass to source plugins
	struct settings app_settings;
	app_settings.jpeg_quality=jpeg_quality;
	app_settings.weighted_gray=weighted_gray;
	app_settings.channels=channels;
	app_settings.verbose=verbose;
;

	unsigned char settings_raw[10000]; //[sizeof(settings)];
	memcpy(settings_raw,(const char *)&app_settings,sizeof(settings)); 

	DSOSource->init(source_parameters, settings_raw);
    //ImageSource->DoSomething();
    //


	//-------------------------
	//Main code
	//-------------------------

	//Socket descriptor: tcp_connection 
	int tsockfd; 
	//Socket descriptor: main
	int sockfd; 
	//Addresses
    struct sockaddr_in servaddr, cliaddr; 
    memset(&servaddr, 0, sizeof(servaddr)); 
    memset(&cliaddr, 0, sizeof(cliaddr)); 

	//Response buffer
    char buffer[MAXLINE]; 

	//UDP MODE	
	if(udp_mode){
	    // Creating socket file descriptor 
	    if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) { 
		    perror("socket creation failed"); 
		    exit(EXIT_FAILURE); 
		} 

		//Experimental: TTL value override
		/*   
		int ttl = 1; 
		// max = 255 /
		 if ( setsockopt(sockfd, IPPROTO_IP, IP_TTL, &ttl, sizeof(ttl))< 0 ) { 
				perror("socket configuration failed"); 
				exit(EXIT_FAILURE); 
			} 
		*/

		// Filling server information 
		servaddr.sin_family    = AF_INET; // IPv4 
		servaddr.sin_addr.s_addr = INADDR_ANY; 
		servaddr.sin_port = htons(port); 
		  
		// Bind the socket with the server address 
		if ( bind(sockfd, (const struct sockaddr *)&servaddr,  
		        sizeof(servaddr)) < 0 )
		{ 
		    perror("bind failed"); 
		    exit(EXIT_FAILURE); 
		} 

    unsigned int n; 
socklen_t len = sizeof cliaddr;
    n = recvfrom(sockfd, (char *)buffer, MAXLINE,  
                MSG_WAITALL, ( struct sockaddr *) &cliaddr, 
                &len); 
    buffer[n] = '\0'; 
	//Debug: Client output
// 	printf("Client : %s\n", buffer); 
//	printf("SOCK: %d\n",sockfd);
//sleep(5);

	//	while (true) {
			streamer(&sockfd, &cliaddr);
	//	sleep(5);		
	//	}

		//close( sockfd ); 
		return 0;

	//TCP MODE
	}else{
		/*
			//Response result   
			//char response [100]="";
			//const char *hello = "Hello from server"; 
			//printf("time: %ld\n",timestamp());
		*/
		// Creating socket file descriptor 
		if ( (tsockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0 ) { 
				perror("socket creation failed"); 
				exit(EXIT_FAILURE); 
		} 

		  
		// Filling server information 
		servaddr.sin_family    = AF_INET; // IPv4 
		servaddr.sin_addr.s_addr = INADDR_ANY; 
		servaddr.sin_port = htons(port); 
		  
		// Bind the socket with the server address 
		if ( bind(tsockfd, (const struct sockaddr *)&servaddr,  
		        sizeof(servaddr)) < 0 )
		{ 
		    perror("bind failed"); 
		    exit(EXIT_FAILURE); 
		} 


		//TCP listen
		if ( listen( tsockfd, 25) < 0) { ;
			close(tsockfd);
			perror( "listen error!\n");
			exit(EXIT_FAILURE); 
		}

		//Process id
		pid_t pid;

		//Accept connections
		while (true) {
			sockfd = accept( tsockfd, NULL, NULL );
			pid = fork();
	//		printf("Accept %d\n", pid);
			if ( pid < 0) {
				perror( "fork error!\n");
			}
			if (pid == 0 ) {
				close( tsockfd );
				//Streaming transaction
				streamer(&sockfd, &cliaddr);
				//sleep(1);
			}

			//printf("Client : %s\n", buffer); 
			//close(sockfd);
	//		return (0);
			close( sockfd ); 
			return 0;
		} 
	//DSO	
	destroy( DSOSource );	
	}
} 
	
