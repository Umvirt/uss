#include <X11/Xlib.h>
#include <X11/X.h>

#include <stdio.h>
#include <X11/Xutil.h>


//jpg cimg plugin
#include <jpeglib.h>
#include <jerror.h>
#define cimg_plugin "plugins/jpeg_buffer.h"
#include "CImg.h"

//variables
unsigned int jpeg_quality = 10;
//channels: 3 - RGB 1 - Grayscale
unsigned int channels = 1;
//weighted gray
bool weighted_gray=1;


using namespace cimg_library;
int main()
{
   Display *display = XOpenDisplay(NULL);
   Window root = DefaultRootWindow(display);

   XWindowAttributes gwa;

   XGetWindowAttributes(display, root, &gwa);
   int width = gwa.width;
   int height = gwa.height;


   XImage *image = XGetImage(display,root, 0,0 , width,height,AllPlanes, ZPixmap);

   unsigned char *array = new unsigned char[width * height * channels];

   unsigned long red_mask = image->red_mask;
   unsigned long green_mask = image->green_mask;
   unsigned long blue_mask = image->blue_mask;

unsigned char R,G,B;
   unsigned long c=0;
      for (int y = 0; y < height ; y++){
   
//		c=0;
for (int x = 0; x < width; x++){
         unsigned long pixel = XGetPixel(image,x,y);

//         unsigned char blue = pixel & blue_mask;
  //       unsigned char green = (pixel & green_mask) >> 8;
    //     unsigned char red = (pixel & red_mask) >> 16;
	//	c++;		
	//	array[c]=pixel>>16;
//		c++;		
//		array[c]=pixel>>8;
		c++;		

		//if rgb
		if(channels==3){
			array[c+(height*width)*2]=pixel;
			array[c+(height*width)*1]=pixel>>8;
			array[c+(height*width)*0]=pixel>>16;
		}
		
		//if grayscale
		if(channels==1){
			R=pixel;
			G=pixel>>8;
			B=pixel>>16;

			if(weighted_gray){
				array[c]=round(0.299*R)+round(0.587*G)+round(0.114*B);
			}else{
				array[c]=round(0.3*R)+round(0.3*G)+round(0.3*B);
			}
		}

         //array[(x + width * y) * 3] = red;
         //array[(x + width * y) * 3+1] = green;
         //array[(x + width * y) * 3+2] = blue;
      }
}
  unsigned buf_size = 500000; // Put the file size here !
  JOCTET *buffer_output = new JOCTET[2*buf_size];

   CImg<unsigned char> pic(array,width,height,1,channels);
//   pic.save_jpeg("screenshot.jpg",jpeg_quality);
pic.save_jpeg_buffer(buffer_output,buf_size,60);

  const char *filename_output = "screenshot_mem.jpg";
  //std::fprintf(stderr," - Save output file '%s'\n",filename_output);
  std::FILE* file_output = std::fopen(filename_output,"wb");
  std::fwrite(buffer_output, sizeof(JOCTET), buf_size, file_output);
  std::fclose(file_output);
  delete[] buffer_output;


   printf("%ld %ld %ld\n",red_mask>> 16, green_mask>>8, blue_mask);

   return 0;
}
