// Client side C/C++ program to demonstrate Socket programming 
#include <stdio.h> 
#include <sys/socket.h> 
#include <stdlib.h> 
#include <time.h>
#include <netinet/in.h> 
#include <string.h> 
#include <sys/uio.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>

#include <getopt.h>

#include <jpeglib.h>
#include <jerror.h>
#define cimg_plugin "plugins/jpeg_buffer.h"
#include "CImg.h"
#include <exception>

using namespace cimg_library;

//=================================
//SDL
//=================================

#ifdef SDL

#include <stdlib.h>
#include <SDL2/SDL.h>
#define WINDOW_WIDTH 600

#endif

//=================================



#include "scr.h"

#define APP_VERSION     "0.3"
#define PORT 8888 

//==============================
//JPEG ERROR HANDLER
//==============================
//https://github.com/zakinster/detiq-t/blob/master/ImageIn/JpgImage.cpp
//https://stackoverflow.com/questions/19857766/error-handling-in-libjpeg
extern "C" {
#include <jpeglib.h>
}
#include <csetjmp>
#include <cstdio>
#include <cstdlib>

struct jpegErrorManager {
    /* "public" fields */
    struct jpeg_error_mgr pub;
    /* for return to caller */
    jmp_buf setjmp_buffer;
};
char jpegLastErrorMsg[JMSG_LENGTH_MAX];
void jpegErrorExit (j_common_ptr cinfo)
{
    /* cinfo->err actually points to a jpegErrorManager struct */
    jpegErrorManager* myerr = (jpegErrorManager*) cinfo->err;
    /* note : *(cinfo->err) is now equivalent to myerr->pub */

    /* output_message is a method to print an error message */
    /*(* (cinfo->err->output_message) ) (cinfo);*/      
    
    /* Create the message */
    ( *(cinfo->err->format_message) ) (cinfo, jpegLastErrorMsg);

    /* Jump to the setjmp point */
    longjmp(myerr->setjmp_buffer, 1);
  
}
//==============================

//global variables
unsigned int jpeg_quality = 90;
//channels: 3 - RGB 1 - Grayscale
unsigned int channels = 1;
//weighted gray
bool weighted_gray=0;
//verbose mode
bool verbose=0;
//binging to port
unsigned int port=PORT;
//host
unsigned int host=INADDR_ANY;
//udp mode
bool udp_mode=0;
//sleep
unsigned int sleeptime=0;
//software rendering mode
bool softwarerender_mode=0;
//dump frames
bool dump_mode=0;
char * dump_dir="./";
//=============================
bool checkjpeg(JOCTET * bytes, struct record * rec){
	return !(bytes[0] == (char)0xff && 
            bytes[1] == (char)0xd8 &&
            bytes[rec->buffer_size-2] == (char)0xff &&
            bytes[rec->buffer_size-1] == (char)0xd9);
//; &&			sizeof(bytes)<sizeof(rec->buffer)			
//			);

}

void helpdump(char *argv[]){
	fprintf(stdout,"UmVirt Screen Client usage: \n\n");
	fprintf(stdout,"	%s <options> \n\n",argv[0]);
	fprintf(stdout,"Client options: \n");
	fprintf(stdout,"--help                          Print this page.\n");
	fprintf(stdout,"--version                       Print version.\n");
	fprintf(stdout,"--dump                       	Dump frames to disk.\n");

	fprintf(stdout,"--verbose                       Verbose mode.\n");
	fprintf(stdout,"--udp-mode                      Connect via UDP against TCP.\n");
	fprintf(stdout,"--host=HOST                     Connect to host.\n");
	fprintf(stdout,"--port=PORT                     Connect to port. (Default: 8888)\n");
	fprintf(stdout,"--dump-dir=DIR                  Path to dump frames directory. (Default: \"./\")\n");
	fprintf(stdout,"--software-render               Use software render against accelerated one.\n");
	fprintf(stdout,"\n");	
	fprintf(stdout,"Server options (Not implemented yet): \n");
	fprintf(stdout,"--channels=<1|3>                Image chanels <1|3> (grayscale|RGB).\n");
	fprintf(stdout,"--weighted-gray                 Weighted gray RGB to grayscale conversion.\n");
	fprintf(stdout,"--jpeg-compression=<1..100>     JPEG compression level.\n");
	fprintf(stdout,"--sleep=SECONDS                 Sleep seconds between frames.\n");
	exit(EXIT_SUCCESS);
}


void printversion(){
	fprintf(stdout,"UmVirt Screen Client v.%s\n",APP_VERSION);
	exit(EXIT_SUCCESS);
}

   
int main(int argc, char *argv[]) 
{ 

	char dump_output[255];

	//--------------
	//Getopt parsing
	//--------------
	int c;
	int digit_optind = 0;
	while (1) {
       int this_option_optind = optind ? optind : 1;
       int option_index = 0;
       static struct option long_options[] = {
           {"weighted_gray",  no_argument, 0, 'w'},
		   {"version",     no_argument, 0,  'v' },	
		   {"dump",     no_argument, 0,  'd' },	                  
			{"help",  no_argument,       0,  'H' },
           {"channels",  required_argument, 0,  'c' },
			{"host",  required_argument, 0,  'h' },
			{"port",  required_argument, 0,  'p' },
			{"dump-dir",  required_argument, 0,  'D' },
			{"sleep",  required_argument, 0,  's' },
			{"software-render",  no_argument, 0,  'S' },
           {"verbose", no_argument,       0,  'V' },
			{"udp-mode", no_argument,       0,  'u' },
           {"jpeg_compression",   required_argument, 0, 'j'},
           {0,         0,                 0,  0 }
       };

       c = getopt_long(argc, argv, "vcjwpVHhdD:",
                long_options, &option_index);
       if (c == -1)
           break;

       switch (c) {
       case 0:
           printf("option %s", long_options[option_index].name);
           if (optarg)
               printf(" with arg %s", optarg);
           printf("\n");
           break;

 /*      case '0':
       case '1':
       case '2':
           if (digit_optind != 0 && digit_optind !=
this_option_optind)
             printf("digits occur in two different argv-ele‐
ments.\n");
           digit_optind = this_option_optind;
           printf("option %c\n", c);
           break;
*/
       case 'a':
           printf("option a\n");
           break;

		case 'v':
			printversion();
			
		break;               
		 case 'V':
           verbose = 1;
           break;				
		case 'w':
           weighted_gray=1;
           break;
		case 'S':
           softwarerender_mode=1;
           break;
		case 'd':
           dump_mode=1;
           break;
       case 'c':
			channels=atoi(optarg);
           break;

       case 'j':
           jpeg_quality = atoi(optarg);
           break;
		case 'u':
           udp_mode=1;
           break;
	 case 'h':
			// Convert IPv4 and IPv6 addresses from text to binary form 
			if(inet_pton(AF_INET, optarg, &host)<=0)  
			{ 
				printf("\nInvalid address/ Address not supported \n"); 
				return -1; 
			} 
           break;
	 case 'p':
           port = (unsigned int)atoi(optarg);
           break;
					 case 's':
                   sleeptime = (unsigned int)atoi(optarg);
                   break;

       case 'D':
           dump_dir = optarg;
           break;
		case 'H':
		case '?':
		   helpdump(argv);
           break;

       default:
           printf("?? getopt returned character code 0%o ??\n", c);
       }
   }
	//--------------
	//Main code
	//--------------


#ifdef SDL

	//sdl window init
    SDL_Event event;
    SDL_Renderer *renderer;
    SDL_Window *window;
	SDL_Texture *texture;
    int i;

    SDL_Init(SDL_INIT_VIDEO);
    SDL_Init(SDL_INIT_VIDEO);
//    SDL_CreateWindowAndRenderer(1200, 1200, 0, &window, &renderer);
int posX = 100, posY = 100, width_ = 320, height_ = 240;
window = SDL_CreateWindow("UmVirt Screen Client", posX, posY, width_, height_, SDL_WINDOW_RESIZABLE);

	if(softwarerender_mode){
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE);
	}else{
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	}

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);
#endif
	//backend init
    struct sockaddr_in dest_addr; 
    int sock = 0, valread; 
    struct sockaddr_in serv_addr; 
    const char *hello = "Hello from client"; 
    struct iovec buffer;
    if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0){ 
        printf("\n Socket creation error \n"); 
        return -1; 
    } 


 
    memset(&serv_addr, '0', sizeof(serv_addr)); 
    serv_addr.sin_family = AF_INET; 
    serv_addr.sin_port = htons(port); 
    serv_addr.sin_addr.s_addr = host;
	
	int sockfd; 

    // Creating socket file descriptor 
	//UDP MODE	
	if(udp_mode){   

	  if ( (sockfd = socket(AF_INET, SOCK_DGRAM  | SOCK_CLOEXEC, IPPROTO_UDP)) < 0 ) { 
         perror("socket creation failed"); 
        exit(EXIT_FAILURE); 
     } 
    // Creating socket file descriptor 
	//TCP MODE
	}else{

	 if ( (sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0 ) { 
         perror("socket creation failed"); 
        exit(EXIT_FAILURE); 
     } 
	
	
	}



	//Connecting to server
	if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0){ 
        printf("\nConnection Failed \n"); 
        return -1; 
    } 

	unsigned int size, channels;
	char buffer2 [1024000];
	socklen_t len = sizeof dest_addr;
	size=sizeof(serv_addr);
    valread = sendto(sockfd , hello , strlen(hello) , MSG_CONFIRM,(struct sockaddr *)&serv_addr, sizeof(serv_addr) );
	if(verbose){
		//printf("REQUEST: %d\n",valread ); 
    	printf("Hello message sent\n"); 
	}

	int width,height,x,y;
	unsigned long int msize,p; 

#ifdef SDL

	SDL_RenderClear(renderer);		
#endif
	JOCTET *buffer_input;






	//MAIN LOOP
	while(1){


#ifdef SDL
		SDL_PollEvent(&event);
                if(event.type == SDL_QUIT){
                        break;
				}
#endif


		struct record rec;
		//read raw binary data
		valread = recvfrom( sockfd , buffer2, sizeof(rec),MSG_WAITALL,(struct sockaddr *)&dest_addr, &len); 
		//copy data to struct
		memcpy(&rec, buffer2, sizeof(rec));

		//valread = recv(sockfd, (char *)buffer2, 3,MSG_WAITALL); 
		int errsv = errno;
		if(verbose){
			printf("Server response length: %d\n",valread );
		}
		if(valread<0){
			printf("ERROR code: %d\n",errno);
			printf("ERROR: %s\n",strerror(errno));
			return -1;
		}

		
		//If first or single segment		
		if(rec.index==0){
			//load buffer contents to JOCTET
			buffer_input = new JOCTET[rec.total_size*2];
			//simple copy all contens of buffer
			memcpy(buffer_input,rec.buffer,rec.buffer_size);	
		}else{
			//if(verbose){
			//	printf("X1 %d %d %d\n",rec.index,rec.buffer_size, sizeof(buffer_input));
			//}
			//append buffer data to JOCTET	
			unsigned long int j;
			for(j = 0; j < rec.buffer_size; j++){
				//if(j>=rec.buffer_size){
				//break;
				//}
		//		printf("%d %d %d\n",j,(rec.index*SCR_TBUFFER_SIZE),j+(rec.index*SCR_TBUFFER_SIZE));
				buffer_input[j+(rec.index*SCR_TBUFFER_SIZE)]=rec.buffer[j];
			}
//if(rec.buffer_size!=SCR_TBUFFER_SIZE){
//return(0);
//}
		}

		
		//Debug: Recieved image content buffer file dump 
		//*
		//const char *filename_output = "client_image_dump";
		//std::FILE* file_output = std::fopen(filename_output,"wb");
		//std::fwrite(buffer_input, rec.total_size,1, file_output);
		//std::fclose(file_output);
		//*/


		if(verbose){
			if(rec.buffer_size!=rec.total_size){
				printf("Segment #%d recieved: buffer_size=%d total_size=%d\n",rec.index, rec.buffer_size, rec.total_size);
			}else{
				printf("Whole image recieved: size=%d\n", rec.total_size);
			}
		}


		
		//If first or final segment
		if(rec.final){

		if(verbose){
			printf("Rendering image: size=%d", rec.total_size);
		}
		//jpeg error handling start
		struct jpeg_decompress_struct cinfo;
 		jpegErrorManager jerr;
    	cinfo.err = jpeg_std_error(&jerr.pub);
    	jerr.pub.error_exit = jpegErrorExit;
		if (setjmp(jerr.setjmp_buffer)) {
			jpeg_destroy_decompress(&cinfo);		
		}
		//jpeg error handling stop

		if(checkjpeg(buffer_input, &rec)){
  //try
  //{			
		

		//create image object
		CImg<unsigned char> img;
		//store channels

		img.load_jpeg_buffer(buffer_input, rec.total_size);

		width=img.width();
		height=img.height();

		channels=img.spectrum();
		
		if(verbose){
			printf(" width=%d",width);
			printf(" height=%d",height);
			printf(" channels=%d\n",channels);
		}
#ifdef SDL
		//create texture
		texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, width, height);
		SDL_SetRenderTarget(renderer, texture);
#endif
		//SDL_RenderCopy(renderer, texture, NULL, NULL);

		//printf("Width: %d\n",width );
		//printf("Height: %d\n",height );

		msize=width*height;

		x=0;
		y=0;
		p=0;

		//const char *filename_output = "foo_output.jpg";
		//  std::fprintf(stderr," - Save output file '%s'\n",filename_output);
		//  std::FILE* file_output = std::fopen(filename_output,"wb");
		//  std::fwrite(rec.buffer, sizeof(JOCTET), rec.buffer_size, file_output);
		//  std::fclose(file_output);
 

		//char *dump_output;
		//dump_output="";
		//dump_dir="";
		//printf("%d-%d\n", rec.time,rec.mtime);
		sprintf(dump_output, "%s%ld-%ld.jpg",dump_dir, rec.time, rec.mtime);
		if(verbose){
		printf("Frame dumped to file: %s\n",dump_output);
		}
		if(dump_mode){
			FILE* file_output = fopen(dump_output,"wb");
			fwrite(buffer_input, rec.total_size, 1, file_output);;
			fclose(file_output);
		}


#ifdef SDL
		for(p= 0; p<=msize; ++p){
			
			if(channels==3){
				SDL_SetRenderDrawColor(renderer, (int)img(x,y,0,0), (int)img(x,y,0,1), (int)img(x,y,0,2), 255);
			}else{		
				SDL_SetRenderDrawColor(renderer, (int)img(x,y,0,0), (int)img(x,y,0,0), (int)img(x,y,0,0), 255);
			}
			SDL_RenderDrawPoint(renderer, x, y);

			++x;

			if(x>=width){
				x=0;
				++y;			
			}	
		}
		//SDL_RenderPresent(renderer);	
//}catch (Exception e){
//		fprintf(stderr,"CImg Library Error : %s",e.what());
//	}

//SDL_RenderClear(renderer);
		
    SDL_SetRenderTarget(renderer, NULL);
	//SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
	//SDL_RenderClear(renderer);
	SDL_RenderCopy(renderer, texture, NULL, NULL);
	SDL_RenderPresent(renderer);
	//SDL_DestroyTexture(texture);
#endif

}else{
	printf("Error: wrong image\n");
}
		
		}
	
//std::fread(buffer_input,sizeof(JOCTET),rec.buffer_size,rec.buffer)
				
	//sleep(1);	
	}
	
	//delete buffer		
	delete[] buffer_input;
#ifdef SDL	
	//Destroy window	
	SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return EXIT_SUCCESS;
#endif

} 
