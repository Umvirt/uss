
#define	SCR_TBUFFER_SIZE     500
//50000 is good for lan
//500 is safe for wan

struct record
{

  //JOCTET * buffer;
	//char * buffer;
	//should be lower than udp packetsize  
    char buffer[SCR_TBUFFER_SIZE];
	unsigned long int buffer_size;
	unsigned long int total_size;
	bool final = 0;
	unsigned int index = 0;
  long time=0;
  long mtime=0;
};

struct settings
{
	//Warning: Bool values not passed with memcpy. Changed to unsined char!
	//verbose mode
	unsigned char verbose;	
	//weighted gray
	unsigned char weighted_gray;
	//jpeg compression
	unsigned char jpeg_quality;
	//channels: 3 - RGB 1 - Grayscale
	unsigned char channels;
	
	//binging to port
	unsigned int port;
	//udp mode
	bool udp_mode;
	//sleep
	unsigned int sleeptime;
	//source settings
	char * source_parameters;
};
