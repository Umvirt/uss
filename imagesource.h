#ifndef __IMAGESOURCE_H__
#define __IMAGESOURCE_H__

class ImageSource
{
public:
  ImageSource();

  /* use virtual otherwise linker will try to perform static linkage */
virtual void init(char * source_parameters, unsigned char settings [10000]);  
virtual void getImage(unsigned char ** buffer_output, unsigned int ** buf_size);

//JOCTET ** buffer_output, unsigned int ** buf_size

private:
  char * parameters;
  struct settings app_settings; 
};

#endif
